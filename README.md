# Mumbai Moonshot Hackathon 11 Oct 2023

## Convert a Salesforce Metadata Pipeline into a Salesforce Source Format Pipeline

-   **Problem:** The migration between pipelines is a tedious and time-consuming process.

-   **Solution:** Creation of a job template that triggers the migration process automatically, so that the customer only needs to perform a couple of manual steps and just click a button.

## Job Template Technical Process

1. It clones the repo for the Metadata Pipeline from the Main branch.
2. It converts a selection of environment branches.
3. It pushes all the work to the new Git repository.
4. It clones the Pipeline record and its connections with the new Git repository created for the Source Format Pipeline.
5. It updates the **Platform** field in the Pipeline/Environment records from **Salesforce** to **SFDX**.

## Customer Steps

### Pre-work

1. Create a Git repo & branches for the Source Format Pipeline.
2. Create a Copado Git Repository record & Connect to the Git repo.
3. Commit all the work in progress to the user story so that the repo branches have the latest content.

### Copado Automation

1. **Migrate Source Format** button in the Pipeline record.
2. Select the Git repositories and click **Submit** (the job template will carry out the migration).

### Post-work

1. Manually recommit the files.

## Other Documentation

-   [Presentation](https://docs.google.com/presentation/d/1dDynsxTaANCwF5zA-fBw-DqsxBKo5O40a64al2wkjqc/edit?usp=sharing)
