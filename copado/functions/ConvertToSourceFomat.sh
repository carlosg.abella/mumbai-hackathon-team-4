#!/bin/bash
source='/app/sourcedirectory'
destination='/app/sfdxrepository'
cd /app
mkdir sourcedirectory
cd sourcedirectory
copado-git-get master
git config --global user.name "carlos"
git config --global user.email "cgarcia@copado.com"
# Fetch all remote branches
git fetch --all
echo "-----------LS "
pwd
cd ..
sfdx force:project:create --projectname sfdxrepository --template empty
cd sfdxrepository
git init
pwd

# Convert the code into an array
branch_array=($(echo "$branches" | jq -r '.[]'))
branch_array+=('master')
echo "-----Iteration"

for branch in "${branch_array[@]}"
do
  echo "----------checkout source"
  cd $source
  git checkout $branch
  pwd
  echo "-----------checkout destination"
  cd $destination
  git checkout -B $branch
  pwd
  sfdx force:mdapi:convert --rootdir $source
  git add .
  git commit -m "Commit branch"
  echo "-----------Finish"
done

cd $destination
git remote add origin https://github.com/carlosabella/source-format.git
git remote -v
git push origin --all