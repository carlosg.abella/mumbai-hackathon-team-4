@SuppressWarnings('PMD.AvoidGlobalModifier')
global class GetGitJson implements copado.ParameterExpressionCallable {
    private static final String GIT_REPOSITORY_ID = 'gitRepositoryId';
    private static final String GIT_CREDENTIAL_DYNAMIC_EXPRESSON = '{$Context.Repository.Credential}';

    // GLOBAL

    global String execute(Id contextId) {
        String result = '';
        try {
            result = getGitJsonValue(contextId);
        } catch (Exception ex) {
            throw ex;
        }
        return result;
    }

    // PRIVATE

    private String getGitJsonValue(Id gitRepositoryId) {
        copado.Jobs.DynamicExpressionEvaluateRequest request = new copado.Jobs.DynamicExpressionEvaluateRequest(
            gitRepositoryId,
            new List<String>{ GIT_CREDENTIAL_DYNAMIC_EXPRESSON }
        );
        List<copado.Jobs.DynamicExpressionEvaluateResult> responses = copado.Jobs.DynamicExpression.evaluate(request);
        return responses[0].value;
    }
}
