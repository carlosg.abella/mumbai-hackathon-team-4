global class pipelineDetails {
   @InvocableMethod(label='Get Pipeline Details' description='Get Pipeline Details')
   public static void getPipelineIds(List<PipelineRequest> pipelineRequestList) {
       List<copado__Deployment_Flow_Step__c> pipelineConnectionList = pipelineRequestList.get(0).deploymentConnection;
       Id gitRepoIdVar = pipelineRequestList.get(0).gitRepoId;
       Id pipelineId = pipelineRequestList.get(0).pipelineId;
       system.debug('*****************'+pipelineConnectionList);
       system.debug('*****************'+gitRepoIdVar);
       system.debug('*****************'+pipelineId);
       Wrapper wrap = new Wrapper();
       List<string> branchList = new List<string>();
       for(copado__Deployment_Flow_Step__c lst : pipelineConnectionList) {
           branchList.add(lst.copado__Branch__c);
       }
       wrap.branches = branchList;
             
       GetGitJson getGitJson = new GetGitJson();
	   String destinationRepository = getGitJson.execute(gitRepoIdVar);
       wrap.destination = destinationRepository;
       String jsonString = JSON.serialize(wrap);
       copado.CreateExecution.Request request = new copado.CreateExecution.Request();
       request.templateName = 'Convert_to_source_format_1'; 
       request.pipelineId = pipelineId; 
       request.dataJson = jsonString;
       request.runAfterInstantiation = true; 
       copado.CreateExecution.execute(new List<copado.CreateExecution.Request>{ request });
   }
   
   public class PipelineRequest{
        
        @InvocableVariable
        public List<copado__Deployment_Flow_Step__c> deploymentConnection;

        @InvocableVariable
        public Id gitRepoId;
       
       @InvocableVariable
        public Id pipelineId;
       
       
    }
    public class wrapper {
        
        public List<string> branches;
        public string destination;
    }
}